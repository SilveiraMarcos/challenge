import React from 'react';
import './Sidebar.css';

export default function Sidebar({title}) { 
    return (
        <div className="bar">
            <h2 className="bigblue">{title}</h2>
        </div>
    );
}