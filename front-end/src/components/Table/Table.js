import React, {useState, useMemo, useEffect} from 'react';
import {BsFillCaretDownFill, BsFillCaretUpFill, BsSearch} from "react-icons/bs";
import './Table.css';

const useSortable = (data, config = null) => {
  const [sortMetadata, setSortMetadata] = useState(config);

  const sortedData = useMemo(() => {
    let sortedData = [...data];
    if (sortMetadata !== null) {
      sortedData.sort((a, b) => {
        if (a[sortMetadata.key] < b[sortMetadata.key]) {
          return sortMetadata.direction === 'ascd' ? -1 : 1;
        }
        if (a[sortMetadata.key] > b[sortMetadata.key]) {
          return sortMetadata.direction === 'ascd' ? 1 : -1;
        }
        return 0;
      });
    }
    return sortedData;
  }, [data, sortMetadata]);

  const setSort = (key) => {
    let direction = (sortMetadata && sortMetadata.key === key && sortMetadata.direction === 'ascd') ? 'desc' : 'ascd';
    setSortMetadata({ key, direction });
  };

  return { data: sortedData, setSort, sortMetadata };
}

export default function Table({columns, rows}) {
  const [searchTerm, setSearchTerm] = useState("");
  const {data, setSort, sortMetadata} = useSortable(rows);
  const [records, setRecords] = useState(rows);

  useEffect(()=>{
      if(searchTerm === ""){
        setRecords(data);
      }
      
  },[searchTerm, data])

  function rowsFilter(){
      let rows = data.filter(item=>{
        return item.name.toLowerCase().includes(searchTerm.toLowerCase());
      });

      setRecords(rows);
  }

  return (
      <div className="table">

        <div className="containerSearch">
          <input
            className="searchInput"
            onChange={(event)=>{
              setSearchTerm(event.target.value)
            }}
            placeholder="Buscar cliente..."/>

          <button className="searchInputButton" onClick={()=> rowsFilter()}><BsSearch/></button>
        </div>
        <table>
            <thead>
                <tr>
                    {
                    
                    columns.map((prop, key) => {
                        return <th onClick={() => {if(prop.sort) setSort(Object.keys(prop)[0])}} key={key} component="th" scope="row">{prop[Object.keys(prop)[0]]}
                                {sortMetadata && prop.sort? <span> {sortMetadata?.direction !== "desc"?  <BsFillCaretDownFill/> : <BsFillCaretUpFill/>}</span> : <></>}
                        </th>;   
                    })}
                </tr>
            </thead>
            <tbody>
              {records.map(row => (
                      <tr key={row._id}>
                        <td>{row.name}</td>
                        <td align="left">{row.cpf_cnpj_client}</td>
                        <td align="left">{row.value}</td>
                        <td align="left">{new Date(row.createdAt).toLocaleString('pt-br')}</td>
                        <td align="left">{row.status}</td>
                      </tr>
              ))}
            </tbody>
        </table>
      </div>
  );
}