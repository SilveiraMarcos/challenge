import React, {useState, useEffect } from 'react';
import Table from '../../components/Table/Table'
import Sidebar from '../../components/Sidebar/Sidebar'
import api from "../../services/api";

export default function Dashboard() { 

    const [clients, setClients] = useState([]);

    const columns = [
      {name: "Nome do Cliente", sort: true}, 
      {cpf_cnpj_client: "CPF/CNPJ", sort: false}, 
      {value: "Valor", sort: true},
      { createdAt: "Desde", sort: true}, 
      {status: "Status", sort: false}
    ]

    useEffect(()=>{
  
      async function loadClients(){
        const response = await api.get("/auth/home");
        setClients(response.data);
      }
      loadClients();
    },[]);
  
  
    return (
      <>
          <Sidebar title={"Clientes Inadimplentes"}></Sidebar>
          <div style={{flexDirection: 'column', display: "flex"}}>
            <div style={{justifyContent: 'center', display: "flex"}}><Table columns={columns} rows={clients}/></div>
          </div>
      </>
    )
}