import React from 'react';

import {BrowserRouter, Switch, Route} from 'react-router-dom';

import DashBoard from './pages/dashboard'

export default function Routes(){
    return(
        <BrowserRouter>
            <Switch>
                <Route path='/home' exact component={DashBoard} />
            </Switch>
        </BrowserRouter>
    )
}