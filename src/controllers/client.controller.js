const express = require('express');
const Client = require('../models/client');
const router = express.Router();

// Metodo para adição dos clientes inadimplentes
router.post('/register', async (req, res) => {
    try{
        const client = await Client.create(req.body);
        return res.send({ client });
    }catch (err){
        return res.status(400).send({ error: 'Erro ao tentar cadastrar: '+ err});
    }
});

router.get('/home', async (req, res) => {
    const client = await Client.find();
    res.json(client);
})

module.exports = app => app.use('/auth', router);