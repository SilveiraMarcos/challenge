
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());
app.use(cookieParser());

require('./controllers/client.controller')(app);

app.listen(port, ()=>{
    console.log(`Server running on port ${port}`);
});